import java.util.Scanner;

public class Activitat3 {
    static final int LIMIT_MOLT_DEFICIENT = 3;
    static final int LIMIT_INSUFICIENT = 5;
    static final int LIMIT_SUFICIENT = 6;
    static final int LIMIT_BE = 7;
    static final int LIMIT_NOTABLE = 9;

    static final int LIMIT_INFERIOR_NOTA_VALIDA = 0;
    static final int LIMIT_SUPERIOR_NOTA_VALIDA = 10;


    public static void main(String[] args) {
        Scanner teclat = new Scanner(System.in);
        System.out.print("Introdueix una nota: ");

        double nota = teclat.nextDouble();

        if (nota < LIMIT_INFERIOR_NOTA_VALIDA || nota > LIMIT_SUPERIOR_NOTA_VALIDA) {
            System.out.println("Nota incorrecta");
        } else if (nota >=LIMIT_INFERIOR_NOTA_VALIDA && nota < LIMIT_MOLT_DEFICIENT) {
            System.out.println("Molt deficient");
        } else if (nota >=LIMIT_MOLT_DEFICIENT && nota < LIMIT_INSUFICIENT) {
            System.out.println("Insuficient");
        } else if (nota >= LIMIT_INSUFICIENT && nota < LIMIT_SUFICIENT) {
            System.out.println("Suficient");
        } else if (nota >= LIMIT_SUFICIENT && nota < LIMIT_BE) {
            System.out.println("Bé");
        } else if (nota >= LIMIT_BE && nota <LIMIT_NOTABLE) {
            System.out.println("Notable");
        } else {
            System.out.println("Excel.lent");
        }
    }
}
