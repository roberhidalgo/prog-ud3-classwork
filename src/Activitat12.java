public class Activitat12 {
    static final int NUMERO_INICIAL_CUENTA_ATRAS = 10;

    public static void main(String[] args) {
        int contador = NUMERO_INICIAL_CUENTA_ATRAS;
         while (contador >= 0) {
             System.out.print(contador);
             if (contador > 0) {
                 System.out.print(",");
             } else {
                 System.out.println("");
             }
             contador--;
         }

        System.out.println("Preparando la partida");
    }
}
