import java.util.Scanner;

public class Activitat20 {

    static final int NUM_TABLAS = 10;
    static final int NUM_OPERACIONES_POR_TABLA = 10;
    public static void main(String[] args) {

        for (int i = 1; i <= NUM_TABLAS; i++) {
            System.out.println("Tabla del " + i);
            System.out.println("-----------");
            for (int j = 1; j <= NUM_OPERACIONES_POR_TABLA; j++) {
                System.out.println(i + " * " + j + " = " + i * j);
            }
        }
    }
}
