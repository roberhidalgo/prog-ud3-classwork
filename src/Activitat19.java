public class Activitat19 {

    public static void main(String[] args) {

        int numPares = 0;
        int numMultiplos5 = 0;

        for (int i = 1; i <= 100; i++) {

            if (i % 2 == 0) {
                System.out.print(i + " ");
                numPares++;
            }
            if (i % 5 == 0) {
                numMultiplos5++;
            }
        }
        System.out.println("\nNúmero de pares: " + numPares);
        System.out.println("Número de múltiplos de 5: " + numMultiplos5);
    }
}
