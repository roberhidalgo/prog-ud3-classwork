import java.util.Scanner;

// Algoritmo: numeroMayor
public class Activitat6 {
    public static void main(String[] args) {

        Scanner teclat = new Scanner(System.in);

        System.out.println("Introdueix un nombre enter");
        int num1 = teclat.nextInt();

        System.out.println("Introdueix altre nombre enter");
        int num2 = teclat.nextInt();


        /* Versión clásica
        if (num1 > num2) {
            cadena = "Número 1 és major";
        } else {
            cadena = "Número 2 és major";
        }*/

        String cadena = (num1 > num2) ? num1 + " és major" : num2 + " es major";
        System.out.println(cadena);

    }

}
