import java.util.Scanner;

public class Activitat4 {
    public static void main(String[] args) {
        Scanner teclat = new Scanner(System.in);
        System.out.print("Introduce una nota: ");

        int nota = teclat.nextInt();
        switch (nota) {
            case 0:
            case 1:
            case 2:
                System.out.println("Molt deficient");
                break;
            case 3:
            case 4:
                System.out.println("Insuficient");
                break;
            case 5:
                System.out.println("Suficient");
                break;
            case 6:
                System.out.println("Bé");
                break;
            case 7:
            case 8:
                System.out.println("Notable");
                break;
            case 9:
            case 10:
                System.out.println("Excel.lent");
                break;
            default:
                System.out.println("Nota incorrecta");
        }
    }
}
