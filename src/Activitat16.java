import java.util.Scanner;

public class Activitat16 {

    public static void main(String[] args) {
        Scanner teclat = new Scanner(System.in);

        System.out.print("Introduce un número: ");
        int numero;

        if (teclat.hasNextInt()) {
            numero = teclat.nextInt();
        } else {
            System.out.println("Tipo incorrecto");
            return;
        }

        if (numero > 0 && numero <= 10) {
            System.out.println("Tabla del " + numero);
            System.out.println("-----------");
            int contador = 1;
            do {
                System.out.println(numero + " * " + contador + " = "
                        + numero * contador);
                contador++;
            } while (contador <= 10);

        } else {
            System.out.println("Número incorrecto");
        }

    }
}
