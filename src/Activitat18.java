import java.util.Scanner;

public class Activitat18 {

    public static void main(String[] args) {
        Scanner teclat = new Scanner(System.in);

        System.out.print("Introduce un número: ");
        int numero;

        if (teclat.hasNextInt()) {
            numero = teclat.nextInt();
        } else {
            System.out.println("Tipo incorrecto");
            return;
        }

        if (numero > 0 && numero <= 10) {
            System.out.println("Tabla del " + numero);
            System.out.println("-----------");

            for (int i = 1; i <= 10; i++) {
                System.out.println(numero + " * " + i + " = "
                        + numero * i);
            }



        } else {
            System.out.println("Número incorrecto");
        }
    }
}
