import java.util.Scanner;

public class Activitat21 {

    public static void main(String[] args) {

        Scanner teclat = new Scanner(System.in);

        for (int i = 0; i < 50; i++) {
            System.out.print("Introduce un número: ");
            int numero = teclat.nextInt();
            if (numero == 100) {
                continue;
            } else if (numero == 0) {
                break;
            } else {
                if (numero % 2 == 0) {
                    System.out.print("Es par");
                } else {
                    System.out.print("Es impar");
                }

                if (numero % 5 == 0) {
                    System.out.println(" y múltiplo de 5");
                } else {
                    System.out.println(" y no es múltiplo de 5");
                }
            }
        }
    }
}
