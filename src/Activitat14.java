public class Activitat14 {

    static final int INICIO_SUMATORIO = 1;
    static final int FINAL_SUMATORIO = 1000;

    public static void main(String[] args) {

        int contador = INICIO_SUMATORIO;
        int sumatorio = 0;

        while (contador <= FINAL_SUMATORIO) {

            sumatorio += contador;
            contador++;
        }
        System.out.println("El sumatorio es " + sumatorio);
    }
}
