public class Activitat17 {
    static final int NUMERO_INICIAL_CUENTA_ATRAS = 10;

    public static void main(String[] args) {

        for (int contador = NUMERO_INICIAL_CUENTA_ATRAS; contador >= 0; contador--) {
            System.out.print(contador);
            if (contador > 0) {
                System.out.print(",");
            } else {
                System.out.println("");
            }
        }

        System.out.println("Preparando la partida");
    }
}
