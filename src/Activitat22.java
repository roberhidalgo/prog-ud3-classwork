public class Activitat22 {

    static final int CARA = 1;
    static final int CARAS_DE_MONEDA = 2;

    static final int NUM_TIRADAS = 1000000;
    public static void main(String[] args) {
        //(int) (Math.random() * 2 + 1)

        int numCaras = 0, numCruces = 0;

        for (int i = 0; i < NUM_TIRADAS; i++) {
            int tirada = (int) (Math.random() * CARAS_DE_MONEDA + 1);

            if (tirada == CARA) {
                numCaras++;
            } else {
                numCruces++;
            }
        }

        System.out.println("Número de tiradas: " + NUM_TIRADAS);
        System.out.println("Número de caras: " + numCaras);
        System.out.println("Número de cruces: " + numCruces);
    }
}
