import java.util.Scanner;

public class Activitat15 {

    static final int VALOR_PARA_FINALIZAR = 10;

    public static void main(String[] args) {

        int numero;
        Scanner teclat = new Scanner(System.in);
        do {
            System.out.println("Introduce el número");
            numero = teclat.nextInt();
            System.out.println(numero);

        } while (numero != VALOR_PARA_FINALIZAR);
    }
}
