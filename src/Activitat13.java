public class Activitat13 {

    static final int INICIO = 48;
    static final int TOPE = 100;
    static final int INCREMENTO = 2;

    public static void main(String[] args) {
        int contador = INICIO;
        while (contador <= TOPE) {
            System.out.print(contador);
            if (contador < TOPE) {
                System.out.print(",");
            } else {
                System.out.println("");
            }
            contador+=INCREMENTO;
        }

        System.out.println("Preparando la partida");
    }
}