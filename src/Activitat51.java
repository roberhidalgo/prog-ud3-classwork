import java.util.Scanner;

// Utilizando switch
public class Activitat51 {

    public static void main(String[] args) {
        Scanner teclat = new Scanner(System.in);

        // Mostrar el menú
        System.out.println("¿Qué tipo de desecho quieres tirar?");
        System.out.println(" 1.Plástico");
        System.out.println(" 2.Orgánico");
        System.out.println(" 3.Papel");
        System.out.println(" 4. Cartón");
        System.out.println(" 5. Otros");

        // Pedir la opción
        System.out.println("Introduce una opción [1-5]:");
        byte opcion = teclat.nextByte();

        // Seleccionar el mensaje a mostrar
        String mensaje = "Debes tirarlo al contenedor ";
        switch (opcion) {
            case 1:
                mensaje += "amarillo";
                break;
            case 2:
                mensaje += "gris";
                break;
            case 3:
            case 4:
                mensaje +="azul";
                break;
            case 5:
                mensaje +="verde";
                break;
            default:
                mensaje = "Opción errónea";
        }

        // Mostrar el mensaje
        System.out.println(mensaje);
    }
}
