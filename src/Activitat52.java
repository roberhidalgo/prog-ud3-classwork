import java.util.Scanner;

// Utilizando if...else anidados
public class Activitat52 {
    public static void main(String[] args) {

        Scanner teclat = new Scanner(System.in);

        // Mostrar el menú
        System.out.println("¿Qué tipo de desecho quieres tirar?");
        System.out.println(" 1.Plástico");
        System.out.println(" 2.Orgánico");
        System.out.println(" 3.Papel");
        System.out.println(" 4. Cartón");
        System.out.println(" 5. Otros");

        // Pedir la opción
        System.out.println("Introduce una opción [1-5]:");
        byte opcion = teclat.nextByte();

        String mensaje = "Debes tirarlo al contenedor ";
        if (opcion == 1) {
            mensaje += "amarillo";
        } else if (opcion == 2) {
            mensaje += "gris";
        } else if (opcion == 3 || opcion == 4) {
            mensaje += "azul";
        } else {
            mensaje += "";
        }
    }
}
