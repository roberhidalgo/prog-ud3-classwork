import java.util.Scanner;

public class Activitat1 {
    static final int LIMITE_MAYOR_EDAD = 18;

    public static void main(String[] args) {
        Scanner teclat = new Scanner(System.in);

        System.out.println("Què edat tens?");
        byte edat = teclat.nextByte();

        if (edat >= LIMITE_MAYOR_EDAD) {
            System.out.println("Eres major d'edat");
        } else {
            System.out.println("Eres menor d'edat");
        }

    }
}
