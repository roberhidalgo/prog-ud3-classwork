import java.util.Scanner;

public class Activitat7 {
    static final double CUOTA_GENERAL = 500.;
    static final double PCTJE_DTO_JUBILADO = 45;
    static final double PCTJE_DTO_MENOR_DE_EDAD = 25;

    static final int EDAD_MINIMA_JUBILADO = 65;
    static final int EDAD_MAXIMA_MAYOR_DE_EDAD = 18;

    public static void main(String[] args) {
        Scanner teclat = new Scanner(System.in);

        System.out.print("Insereix nom: ");
        String nombre = teclat.nextLine();
        System.out.print("Insereix edat: ");
        byte edad = teclat.nextByte();

        double aPagar;
        if (edad > EDAD_MINIMA_JUBILADO) {
            aPagar = CUOTA_GENERAL - (CUOTA_GENERAL * PCTJE_DTO_JUBILADO / 100);
        } else if (edad >= 0 && edad < EDAD_MAXIMA_MAYOR_DE_EDAD) {
            aPagar = CUOTA_GENERAL - (CUOTA_GENERAL * PCTJE_DTO_MENOR_DE_EDAD / 100);
        } else {
            aPagar = CUOTA_GENERAL;
        }

        System.out.println(nombre + ", has de pagar " + aPagar + " euros.");
    }
}
