import java.util.Scanner;

public class Activitat2 {
    public static void main(String[] args) {
        Scanner teclat = new Scanner(System.in);
        System.out.print("Dime un número: ");
        int numero = teclat.nextInt();

        if (numero > 0) {
            System.out.print("El número es positivo");
        } else if (numero < 0){
            System.out.print("El número es negativo");
        } else {
            System.out.println("El número es 0");
        }

        if (numero % 2 == 0) {
            System.out.println(" y es par");
        } else {
            System.out.println(" y es impar");
        }
    }
}
